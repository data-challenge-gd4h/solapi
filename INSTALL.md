# Installation Guide

## Dependencies

To run the project locally, the following dependencies are required:

- Python 3.10 (Python 2 is not supported) 🐍

To install the necessary dependencies:

1. Ensure Python 3.10 is installed on your system. You can download it from the official Python website: [python.org](https://www.python.org/downloads/) 🌐

2. Clone the project repository.
    ```bash
    git clone https://gitlab.com/data-challenge-gd4h/solapi.git
    ```

3. Open a terminal and navigate to the project directory.

4. Install the project dependencies by running the following command:
   ```bash
   pip install -r requirements.txt
   ```

   This will install all the required libraries and packages specified in the `requirements.txt` file.
