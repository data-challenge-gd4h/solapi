# Challenge GD4H - SolAPI

*For an english translated version of this file, follow this [link](/README.en.md)*

Le <a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) est une offre de service incubée au sein de l’ECOLAB, laboratoire d’innovation pour la transition écologique du Commissariat Général au Développement Durable.

Dans ce cadre, un challenge permettant le développement d’outils ancrés dans la communauté de la donnée en santé-environnement afin d’adresser des problématiques partagées a été organisé en 2023.
 
<a href=" https://gd4h.ecologie.gouv.fr/defis" target="_blank" rel="noreferrer">Site</a> 

## SolAPI

L’agriculture urbaine connaît un essor important ces dernières années. La question de la contamination des sols et des végétaux est une des principales préoccupations et un facteur limitant au développement et au lancement des projets.

Les études coûtent cher et les résultats sont propriété des commanditaires qui n’ouvrent pas tous leurs données.
Des données existent pourtant, mais les données brutes issues de missions d’expertise ne servent actuellement qu’aux projets desquelles elles sont issues.

<a href="https://gd4h.ecologie.gouv.fr/defis/800" target="_blank" rel="noreferrer">En savoir plus sur le défi</a>

## **Documentation**

Ces scripts automatisent le transfert des données brutes d'analyses de laboratoire vers des fichiers d'entrée de bases de données nationales, simplifiant ainsi l'intégration des données. 🔄🔬

Le code permet le transfert des données de contamination des sols (Agrolab) et des plantes (Phytocontrol) vers les bases de données BAPPET, BAPPOP et BDSolU. 🌱📊

Au cours du processus, une base de données interne est créée, pouvant être complétée manuellement avec des données terrain pour enrichir l'analyse. Ces données supplémentaires seront également transférées vers les fichiers d'entrée des bases de données nationales. 💡📂

Le code de ces scripts peut être adapté pour s'adapter aux données brutes d'autres laboratoires et à d'autres bases de données nationales, principalement dans le domaine de la contamination des sols et des plantes. 🌍❗

Ce code est open source et a été développé bénévolement par des ingénieurs data. Nous apprécierions vos commentaires et serions ravis d'échanger sur le sujet. Pour toute remarque, veuillez nous contacter à l'adresse suivante : contact@securagri.fr. 📧🤝

Ci-dessous un exemple Mermaid pour illustrer le flux de traitement des données :
```mermaid
graph LR
A[Données brutes]
B(Extraction)
C(Transformation)
D(Chargement)
E[Sortie]
A --> B
B --> C
C --> D
D --> E
```
Cet exemple illustre le flux de traitement des données, de l'extraction à la sortie finale des données traitées. 👩‍💻🔀
### **Installation**

[Guide d'installation](/INSTALL.md)

### **Utilisation**

Pour lancer le programme :

1. Ouvrez un terminal et naviguez vers le répertoire du projet.
2. Exécutez la commande suivante :
   ```python main.py [arguments]```

   Remplacez `[arguments]` par les chemins de fichiers et les emplacements de dossiers respectifs.

   La table suivante décrit les arguments :

   | Argument                | Description                                                 |
   | ----------------------- | ----------------------------------------------------------- |
   | `--agrolab-file`        | Nom du fichier Agrolab (par défaut : `agrolab_data.xlsx`) |
   | `--phytocontrol-file`   | Nom du fichier Phytocontrol (par défaut : `phytocontrol_data.xlsx`) |
   | `--intermediate-folder` | Dossier intermédiaire (par défaut : `data/intermediate`)    |
   | `--output-folder`       | Dossier de sortie (par défaut : `data/output`)              |
   | `--intermediate-only`   | Stop execution after intermediate step                       |

   Assurez-vous de fournir les chemins de fichiers et les emplacements de dossiers appropriés lors de l'exécution de la commande. 📂💡



### **Exemple d'usage**

- Création de fichiers "données brutes laboratoires" sol et plantes sous forme de deux fichiers Excel adaptés au code, contacter contact@securagri.fr si besoin d'une aide pour la création de ces fichiers.

- Exemple de lancement de la première partie du script pour créer la base de données intermédiaire :

=> python main.py --agrolab-file data/agrolab_data.xlsx --phytocontrol-file data/phytocontrol_data.xlsx

- Remplissage de la base de données intermédiaire créée avec les données terrain complétant les données d'analyses brutes "src/data/intermediate.xlsx"

- Exemple de lancement de la deuxième partie du script pour créer les fichiers de transfert de données à BDSolU et BAPPET BAPPOP à partir du fichier intermédiaire :

=> python main.py --agrolab-file agrolab_data.xlsx --phytocontrol-file phytocontrol_data.xlsx --output-folder data/output --intermediate-only

- Les deux fichiers finaux ont été créés et sont placés dans .data/output

### **Contributions**

Si vous souhaitez contribuer à ce projet, merci de suivre les [recommendations](/CONTRIBUTING.md).

### **Licence**

Le code est publié sous licence [MIT](/licence.MIT).

Les données référencés dans ce README et dans le guide d'installation sont publiés sous [Etalab Licence Ouverte 2.0](/licence.etalab-2.0).
