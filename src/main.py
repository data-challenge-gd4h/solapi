import argparse
import pandas as pd

from A_extract import agrolab, phytocontrol
from B_transform import main as transform
from C_load import main as load


def parse_args():
    parser = argparse.ArgumentParser(description="ETL script")
    parser.add_argument(
        "--agrolab-file",
        dest="agrolab_file",
        default="labsol_data.xlsx",
        help="Agrolab file name",
    )
    parser.add_argument(
        "--phytocontrol-file",
        dest="phytocontrol_file",
        default="labveg_data.xlsx",
        help="Phytocontrol file name",
    )
    parser.add_argument(
        "--intermediate-folder",
        dest="intermediate_folder",
        default="data/intermediate",
        help="Intermediate folder",
    )
    parser.add_argument(
        "--output-folder",
        dest="output_folder",
        default="data/output",
        help="Output folder",
    )
    parser.add_argument(
        "--intermediate-only",
        dest="intermediate_only",
        action="store_true",
        default=False,
        help="Stop execution after intermediate step",
    )
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()

    lexique_path = "data/lexique/lexique.xlsx"
    df_lexique = pd.read_excel(lexique_path)

    if not args.intermediate_only:
        ### A - Extract ###
        a = agrolab.AgrolabFile(args.agrolab_file)
        df_agrolab = a.extract()

        p = phytocontrol.PhytocontrolFile(args.phytocontrol_file)
        df_phytocontrol = p.extract()

        ### B - Transform ###
        (
            df_sites,
            df_sites_id_externes,
            df_mailles,
            df_echantillons_agrolab,
            df_echantillons_phytocontrol,
            df_concentrations_agrolab,
            df_concentrations_phytocontrol,
        ) = transform.run(df_agrolab, df_phytocontrol, df_lexique)

        # Export intermediate db into excel file in specified folder
        transform.export_csv(
            df_sites,
            df_sites_id_externes,
            df_mailles,
            df_echantillons_agrolab,
            df_echantillons_phytocontrol,
            df_concentrations_agrolab,
            df_concentrations_phytocontrol,
            args.intermediate_folder,
            "intermediate.xlsx",
        )
    else:
        print("Intermediate step skipped as per user's request.")
        ### C - Load ###
        load.run(df_lexique, args.intermediate_folder, args.output_folder)
