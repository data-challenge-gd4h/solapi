import pandas as pd


# Robust function to real Excel file
def read_excel(path: str, sheet_name: str = None) -> pd.DataFrame:
    # Read Excel file
    try:
        df = pd.read_excel(path, sheet_name=sheet_name)
    except Exception as e:
        print(f"Error when reading Excel file: {e}")
        return None

    # Check if the Excel file is empty
    if df.empty:
        print("Error: the Excel file is empty")
        return None

    return df


def obtain_data(
    bdd_interne_df_veg: pd.DataFrame, bdd_interne_df_sol: pd.DataFrame
) -> list:
    data_veg = []
    data_sol = []

    for _, row in bdd_interne_df_veg.iterrows():
        for column in bdd_interne_df_veg.columns:
            if column.startswith("c_"):
                # Check if concentration is present
                if not pd.isnull(row[column]):
                    # Extract concentration value & name of the contaminant
                    contaminant = column[2:]
                    concentration_c = (
                        str(row[column]).replace("<", "").replace(">", "").strip()
                    )
                    concentration_c = (
                        concentration_c if concentration_c not in ("nan", "") else 0
                    )
                    concentration_s = (
                        str(row[column.replace("c_", "s_")])
                        .replace("<", "")
                        .replace(">", "")
                        .strip()
                    )
                    concentration_s = (
                        concentration_s if concentration_s not in ("nan", "") else 0
                    )
                    concentration = float(concentration_c) + float(concentration_s)

                    # get the id_site & id_maille from the row as well
                    id_site = str(row["id_site"]).strip()
                    id_maille = str(row["id_maille"]).strip()

                    # Also get the ref_echantillon_labveg
                    ref_echantillon_labveg = row["ref_echantillon_labveg"]

                    # Append data to the list
                    data_veg.append(
                        {
                            "id_site": id_site,
                            "id_maille": id_maille,
                            "ref_echantillon_labveg": ref_echantillon_labveg,
                            "Elément contaminant": contaminant,
                            "Moyenne Plante (mg/kg)": concentration,
                        }
                    )

    for _, row in bdd_interne_df_sol.iterrows():
        for column in bdd_interne_df_sol.columns:
            if column.startswith("c_"):
                # Check if concentration is present
                if not pd.isnull(row[column]):
                    # Extract concentration value & name of the contaminant
                    contaminant = column[2:]
                    concentration_c = (
                        str(row[column]).replace("<", "").replace(">", "").strip()
                    )
                    concentration_c = (
                        concentration_c if concentration_c not in ("nan", "") else 0
                    )
                    concentration_s = (
                        str(row[column.replace("c_", "s_")])
                        .replace("<", "")
                        .replace(">", "")
                        .strip()
                    )
                    concentration_s = (
                        concentration_s if concentration_s not in ("nan", "") else 0
                    )
                    concentration = float(concentration_c) + float(concentration_s)

                    # get the id_site & id_maille from the row as well
                    id_site = row["id_site"].strip()
                    id_maille = row["id_maille"].strip()

                    # Also get the ref_echantillon_labveg
                    ref_echantillon_labsol = row["ref_echantillon_labsol"]

                    # Append data to the list
                    data_sol.append(
                        {
                            "id_site": id_site,
                            "id_maille": id_maille,
                            "ref_echantillon_labsol": ref_echantillon_labsol,
                            "Elément contaminant": contaminant,
                            "Moyenne Milieu": concentration,
                        }
                    )

    return data_veg


def retrieve_other_columns(
    input_path: str, data_df: pd.DataFrame, df_lexique: pd.DataFrame
) -> pd.DataFrame:
    # Récupération des données
    df_veg = pd.read_excel(input_path, sheet_name="echantillons_veg")
    df_analyse_veg = pd.read_excel(input_path, sheet_name="analyses_veg")
    df_sol = pd.read_excel(input_path, sheet_name="echantillons_sol")
    df_analyse_sol = pd.read_excel(input_path, sheet_name="analyses_sol")
    df_sites = pd.read_excel(input_path, sheet_name="sites")
    df_mailles = pd.read_excel(input_path, sheet_name="mailles")
    df_sites["id_maille"] = df_sites.id_site.apply(
        lambda x: df_mailles[df_mailles.id_site == x].id_maille.iloc[0]
    )

    veg_dict = df_veg.to_dict(orient="records")
    veg_dict = {d["ref_echantillon_labveg"]: d for d in veg_dict}

    sol_dict = df_sol.to_dict(orient="records")
    sol_dict = {d["id_maille"]: d for d in sol_dict}

    analyse_veg_dict = df_analyse_veg.to_dict(orient="records")
    analyse_veg_dict = {d["ref_echantillon_labveg"]: d for d in analyse_veg_dict}

    analyse_sol_dict = df_analyse_sol.to_dict(orient="records")
    analyse_sol_dict = {d["id_maille"]: d for d in analyse_sol_dict}

    # Ajout des colonnes
    data_df.id_site = data_df.id_site.apply(lambda x: x.strip())
    data_df.id_maille = data_df.id_maille.apply(lambda x: x.strip())

    # Vert
    data_df["Type plante"] = data_df.apply(
        lambda x: veg_dict[x.ref_echantillon_labveg]["type_leg"], axis=1
    )
    data_df["Espece"] = data_df.apply(
        lambda x: veg_dict[x.ref_echantillon_labveg]["espece_leg"], axis=1
    )
    data_df["Variete"] = data_df.apply(
        lambda x: veg_dict[x.ref_echantillon_labveg]["variete_leg"], axis=1
    )
    data_df["Nb échantillons"] = data_df.apply(
        lambda x: veg_dict[x.ref_echantillon_labveg]["nb_echantillons_leg"], axis=1
    )
    data_df["MS ou MF"] = None
    data_df["Moyenne Plante (mg/kg)"] = data_df.apply(
        lambda x: str(
            analyse_veg_dict[x.ref_echantillon_labveg][f"s_{x['Elément contaminant']}"]
        ).replace("nan", "")
        + str(
            round(
                analyse_veg_dict[x.ref_echantillon_labveg][
                    f"c_{x['Elément contaminant']}"
                ],
                5,
            )
        ),
        axis=1,
    )
    data_df["Ecart-type Plante"] = None
    data_df["Min Plante"] = None
    data_df["Max Plante"] = None
    data_df["Mediane Plante"] = None
    data_df["Limite détection plante"] = data_df["Elément contaminant"].apply(
        lambda x: df_lexique[
            df_lexique.nom_molecule_normalized == x
        ].limite_quantif_labveg.iloc[0]
        if x in df_lexique.nom_molecule_normalized.values
        else None
    )
    data_df["Limite quantification plante"] = data_df["Elément contaminant"].apply(
        lambda x: df_lexique[
            df_lexique.nom_molecule_normalized == x
        ].limite_quantif_labveg.iloc[0]
        if x in df_lexique.nom_molecule_normalized.values
        else None
    )
    data_df["BCF"] = None
    data_df["Ecart-type BCF"] = None
    data_df["BCF min"] = None
    data_df["BCF max"] = None

    data_df["Organe analysé"] = data_df.apply(
        lambda x: veg_dict[x.ref_echantillon_labveg]["organe_analyse_leg"], axis=1
    )
    data_df["Lavage/Pelage"] = data_df.apply(
        lambda x: veg_dict[x.ref_echantillon_labveg]["lavage_pelage_leg"], axis=1
    )
    data_df["Maturite"] = data_df.apply(
        lambda x: veg_dict[x.ref_echantillon_labveg]["maturite_leg"], axis=1
    )
    data_df["Stade prélèvement"] = data_df.apply(
        lambda x: veg_dict[x.ref_echantillon_labveg]["stade_prelevement_leg"], axis=1
    )
    data_df["MS (%)"] = data_df.apply(
        lambda x: veg_dict[x.ref_echantillon_labveg]["matiere_seche_leg"], axis=1
    )
    data_df["Unité"] = data_df.apply(
        lambda x: veg_dict[x.ref_echantillon_labveg]["unite_veg"], axis=1
    )
    data_df["Equation Modèle"] = None
    data_df["R2 Modèle"] = None

    # Jaune
    data_df["Nature Sol"] = data_df.apply(
        lambda x: sol_dict[x.id_maille]["type_sol"], axis=1
    )
    data_df["Nature Sous-sol"] = data_df.apply(
        lambda x: sol_dict[x.id_maille]["nature_sous_sol"], axis=1
    )
    data_df["Argile (%)"] = data_df.apply(
        lambda x: sol_dict[x.id_maille]["pourcentages_argiles_sol"], axis=1
    )
    data_df["Sable (%)"] = data_df.apply(
        lambda x: sol_dict[x.id_maille]["pourcentage_sables_sol"], axis=1
    )
    data_df["Limon (%)"] = data_df.apply(
        lambda x: sol_dict[x.id_maille]["pourcentage_limons_sol"], axis=1
    )
    data_df["pH"] = data_df.apply(lambda x: sol_dict[x.id_maille]["ph_sol"], axis=1)
    data_df["Matière organique (%)"] = data_df.apply(
        lambda x: sol_dict[x.id_maille]["matiere_organique_sol"], axis=1
    )
    data_df["C organique (%)"] = data_df.apply(
        lambda x: sol_dict[x.id_maille]["carbone_organique_sol"], axis=1
    )
    data_df["CEC (cmol/kg)"] = data_df.apply(
        lambda x: sol_dict[x.id_maille]["cec_sol"], axis=1
    )

    # Marron
    data_df["Type de milieu (unités)"] = data_df.apply(
        lambda x: sol_dict[x.id_maille]["unite_sol"], axis=1
    )
    data_df["Extraction"] = data_df.apply(
        lambda x: sol_dict[x.id_maille]["extraction_sol"], axis=1
    )
    data_df["Extractants"] = data_df.apply(
        lambda x: sol_dict[x.id_maille]["extractant_sol"], axis=1
    )
    data_df["Moyenne Milieu"] = data_df.apply(
        lambda x: str(
            analyse_sol_dict[x.id_maille][f"s_{x['Elément contaminant']}"]
        ).replace("nan", "")
        + str(analyse_sol_dict[x.id_maille][f"c_{x['Elément contaminant']}"]).replace(
            "nan", ""
        ),
        axis=1,
    )
    data_df["Ecart-type Milieu"] = None
    data_df["Min Milieu"] = None
    data_df["Max Milieu"] = None
    data_df["Mediane Milieu"] = None
    data_df["Limite détection Milieu"] = data_df["Elément contaminant"].apply(
        lambda x: df_lexique[
            df_lexique.nom_molecule_normalized == x
        ].limite_quantif_labsol.iloc[0]
        if x in df_lexique.nom_molecule_normalized.values
        else None
    )
    data_df["Limite quantification Milieu"] = data_df["Elément contaminant"].apply(
        lambda x: df_lexique[
            df_lexique.nom_molecule_normalized == x
        ].limite_quantif_labsol.iloc[0]
        if x in df_lexique.nom_molecule_normalized.values
        else None
    )

    # Violet
    data_df["Type expérimental"] = data_df.id_site.apply(
        lambda x: df_sites[df_sites.id_site == x].type_experimentation.iloc[0]
    )
    data_df["Contexte de l'expérimentation"] = data_df.id_site.apply(
        lambda x: df_sites[df_sites.id_site == x].contexte_experimentation.iloc[0]
    )
    data_df["Origine de la pollution"] = data_df.id_site.apply(
        lambda x: df_sites[df_sites.id_site == x].origine_pollution.iloc[0]
    )
    data_df["Commentaires"] = data_df.id_site.apply(
        lambda x: df_sites[df_sites.id_site == x].commentaires_site.iloc[0]
    )

    # Mauve
    data_df["Auteur1"] = None
    data_df["Auteur2"] = None
    data_df["Titre Article"] = None
    data_df["Année"] = None
    data_df["Journal"] = None
    data_df["Volume"] = None
    data_df["Pages"] = None
    data_df["DOI"] = None
    data_df["Pays"] = None
    data_df["Nature"] = None

    # Suppression des colonnes en trop
    data_df.drop(columns=["ref_echantillon_labsol"], inplace=True)

    return data_df


def write_data_to_excel(
    data: list, df_lexique: pd.DataFrame, input_path: str, output_path: str
):
    # Read the existing Excel file into a DataFrame
    df = pd.read_excel(input_path)

    # Convert the data list to a DataFrame
    data_df = pd.DataFrame(data)
    data_df_final = retrieve_other_columns(input_path, data_df, df_lexique)

    # Add the new data to the existing DataFrame
    # updated_df = pd.concat([df, data_df_final], ignore_index=True)

    # Save the updated DataFrame to the output Excel file
    data_df_final.to_excel(output_path, index=False)
    print(f"Dataframes exported to {output_path}")
