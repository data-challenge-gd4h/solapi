import os
import pandas as pd

from constants import (
    BDSOLU_SITE_COLUMNS,
    BDSOLU_SITE_IDENTIFIANTS_EXTERNES_COLUMNS,
    BDSOLU_SONDAGE_COLUMNS,
    BDSOLU_SONDAGE_NIVEAU_COLUMNS,
    BDSOLU_SONDAGE_NIVEAU_MATERIAU_COLUMNS,
    BDSOLU_ECHANTILLON_COLUMNS,
    BDSOLU_ANALYSE_COLUMNS,
)


# Robust function to real Excel file
def read_excel(path: str, sheet_name: str = None) -> pd.DataFrame:
    # Read Excel file
    try:
        df = pd.read_excel(path, sheet_name=sheet_name)
    except Exception as e:
        print(f"Error when reading Excel file: {e}")
        return None

    # Check if the Excel file is empty
    if df.empty:
        print("Error: the Excel file is empty")
        return None

    return df


def fill_site(input_path: str) -> pd.DataFrame:
    df_intermediate = pd.read_excel(input_path, sheet_name="sites")
    df = pd.DataFrame(columns=BDSOLU_SITE_COLUMNS.keys())

    for bdsolu_col, intermediate_col in BDSOLU_SITE_COLUMNS.items():
        if intermediate_col is not None:
            df[bdsolu_col] = df_intermediate[intermediate_col]

    return df


def fill_sondage(input_path: str) -> pd.DataFrame:
    df_intermediate_maille = pd.read_excel(input_path, sheet_name="mailles")
    df_intermediate_ech_sol = pd.read_excel(input_path, sheet_name="echantillons_sol")
    df_intermediate = pd.merge(
        df_intermediate_maille, df_intermediate_ech_sol, on=["id_site", "id_maille"]
    )
    df = pd.DataFrame(columns=BDSOLU_SONDAGE_COLUMNS.keys())

    for bdsolu_col, intermediate_col in BDSOLU_SONDAGE_COLUMNS.items():
        if intermediate_col is not None:
            df[bdsolu_col] = df_intermediate[intermediate_col]

    return df


def fill_sondage_niveau(input_path: str) -> pd.DataFrame:
    df_intermediate_maille = pd.read_excel(input_path, sheet_name="mailles")
    df_intermediate_ech_sol = pd.read_excel(input_path, sheet_name="echantillons_sol")
    df_intermediate = pd.merge(
        df_intermediate_maille, df_intermediate_ech_sol, on=["id_site", "id_maille"]
    )

    df = pd.DataFrame(columns=BDSOLU_SONDAGE_NIVEAU_COLUMNS.keys())

    for bdsolu_col, intermediate_col in BDSOLU_SONDAGE_NIVEAU_COLUMNS.items():
        if intermediate_col is not None:
            df[bdsolu_col] = df_intermediate[intermediate_col]

    return df


def fill_sondage_niveau_materiau(input_path: str) -> pd.DataFrame:
    df_intermediate_maille = pd.read_excel(input_path, sheet_name="mailles")
    df_intermediate_ech_sol = pd.read_excel(input_path, sheet_name="echantillons_sol")
    df_intermediate = pd.merge(
        df_intermediate_maille, df_intermediate_ech_sol, on=["id_site", "id_maille"]
    )

    df = pd.DataFrame(columns=BDSOLU_SONDAGE_NIVEAU_MATERIAU_COLUMNS.keys())

    for bdsolu_col, intermediate_col in BDSOLU_SONDAGE_NIVEAU_MATERIAU_COLUMNS.items():
        if intermediate_col is not None:
            df[bdsolu_col] = df_intermediate[intermediate_col]

    return df


def fill_echantillon(input_path: str) -> pd.DataFrame:
    df_intermediate_maille = pd.read_excel(input_path, sheet_name="mailles")
    df_intermediate_ech_sol = pd.read_excel(input_path, sheet_name="echantillons_sol")
    df_intermediate = pd.merge(
        df_intermediate_maille, df_intermediate_ech_sol, on=["id_site", "id_maille"]
    )

    df = pd.DataFrame(columns=BDSOLU_ECHANTILLON_COLUMNS.keys())

    for bdsolu_col, intermediate_col in BDSOLU_ECHANTILLON_COLUMNS.items():
        if intermediate_col is not None:
            if type(intermediate_col) is list:
                df_intermediate.date_echantillonnage = (
                    df_intermediate.date_echantillonnage.apply(lambda x: x.date())
                )
                df[bdsolu_col] = df_intermediate.apply(
                    lambda x: " ".join([str(x[col]) for col in intermediate_col]),
                    axis=1,
                )
            else:
                df[bdsolu_col] = df_intermediate[intermediate_col]

    return df


def fill_analyse(input_path: str) -> pd.DataFrame:
    df_intermediate_analyses_sol = pd.read_excel(input_path, sheet_name="analyses_sol")

    # Partie 1
    # Récuper les tuples (id_site, id_maille, élément dont concentration non nulle)
    concentration_columns = [
        col for col in df_intermediate_analyses_sol.columns if "c_" in col
    ]
    df_intermediate_analyses_sol = df_intermediate_analyses_sol[
        ["id_site", "id_maille"] + concentration_columns
    ]

    frames = []
    for col in concentration_columns:
        df_element = df_intermediate_analyses_sol[
            df_intermediate_analyses_sol[col].notnull()
        ][["id_site", "id_maille"]]
        df_element["element_analyse"] = col
        df_element = df_element.drop_duplicates()
        frames.append(df_element)

    df_init = pd.concat(frames)

    # Partie 2
    # Remplir les autres colonnes

    # All columns without the 3 first
    df_new_columns = pd.DataFrame(columns=BDSOLU_ANALYSE_COLUMNS)
    df = pd.concat([df_init, df_new_columns])

    # Retrieve data from intermediate file
    df_intermediate_ech_sol = pd.read_excel(input_path, sheet_name="echantillons_sol")
    infos_by_maille = {
        d["id_maille"]: {
            "identifiant_echantillon_bdsolu": d["identifiant_echantillon_bdsolu"],
            "siret_labsol": d["siret_labsol"],
            "ref_echantillon_labsol": d["ref_echantillon_labsol"],
            "date_analyse_labsol": d["date_analyse_labsol"],
            "laboratoire_labsol": d["laboratoire_labsol"],
            "fraction_analysee_sol": d["fraction_analysee_sol"],
            "methode_preparation_analyse_un_bdsolu": d[
                "methode_preparation_analyse_un_bdsolu"
            ],
        }
        for d in df_intermediate_ech_sol.to_dict(orient="records")
    }

    df["Identifiant de l'échantillon"] = df.id_maille.apply(
        lambda x: infos_by_maille[x]["identifiant_echantillon_bdsolu"]
    )
    df["Code d’identification de l’organisme responsable de l’analyse"] = (
        df.id_maille.apply(lambda x: infos_by_maille[x]["siret_labsol"])
    )
    df[
        "Identifiant unique de l'analyse attribué par le laboratoire effectuant l'analyse"
    ] = df.id_maille.apply(lambda x: infos_by_maille[x]["ref_echantillon_labsol"])
    df["Date d'édition du rapport d'analyse"] = df.id_maille.apply(
        lambda x: infos_by_maille[x]["date_analyse_labsol"]
    )
    df["Lieu d'analyse"] = df.id_maille.apply(
        lambda x: infos_by_maille[x]["laboratoire_labsol"]
    )
    df["Fraction analysée"] = df.id_maille.apply(
        lambda x: infos_by_maille[x]["fraction_analysee_sol"]
    )
    df["Méthode de préparation ou d'analyse 1"] = df.id_maille.apply(
        lambda x: infos_by_maille[x]["methode_preparation_analyse_un_bdsolu"]
    )
    df["Méthode de préparation ou d'analyse 2"] = df.id_maille.apply(
        lambda x: infos_by_maille[x]["methode_preparation_analyse_deux_bdsolu"]
    )
    df["Nom de la ou des méthode(s) de préparation ou d’analyse 1"] = (
        df.id_maille.apply(
            lambda x: infos_by_maille[x]["nom_preparation_analyse_un_bdsolu"]
        )
    )
    df["Nom de la ou des méthode(s) de préparation ou d’analyse 2"] = (
        df.id_maille.apply(
            lambda x: infos_by_maille[x]["nom_preparation_analyse_deux_bdsolu"]
        )
    )

    df["Incertitude d'analyse (%)"] = df.id_maille.apply(
        lambda x: infos_by_maille[x]["incertitude_analyse_labsol"]
    )

    df["Commentaire"] = df.id_maille.apply(
        lambda x: infos_by_maille[x]["commentaires_analyses"]
    )

    # Read analyses sol
    df_analyses_sol = pd.read_excel(input_path, sheet_name="analyses_sol")
    df["Code remarque"] = df.apply(
        lambda x: df_analyses_sol[df_analyses_sol.id_maille == x.id_maille][
            x.element_analyse.replace("c_", "s_")
        ].iloc[0],
        axis=1,
    )
    df["Résultat"] = df.apply(
        lambda x: df_analyses_sol[df_analyses_sol.id_maille == x.id_maille][
            x.element_analyse
        ].iloc[0],
        axis=1,
    )

    # Read lexique tab
    df_lex = pd.read_excel(input_path, sheet_name="lexique_conc")
    lex_dict = {
        d["concentration"]: {
            "sandre_cas_molecule": d["sandre_cas_molecule"],
            "limite_quantif_labsol": d["limite_quantif_labsol"],
            "unite_resultat_labsol": d["unite_resultat_labsol"],
        }
        for d in df_lex.to_dict(orient="records")
    }
    df["Code paramètre"] = df.element_analyse.apply(
        lambda x: lex_dict.get(x)["sandre_cas_molecule"] if lex_dict.get(x) else None
    )
    df["LQI"] = df.element_analyse.apply(
        lambda x: lex_dict.get(x)["limite_quantif_labsol"] if lex_dict.get(x) else None
    )
    df["Unité résultat"] = df.element_analyse.apply(
        lambda x: lex_dict.get(x)["unite_resultat_labsol"] if lex_dict.get(x) else None
    )
    return df


def fill_site_id_externes(input_path: str) -> pd.DataFrame:
    df_intermediate = pd.read_excel(input_path, sheet_name="site_identifiants_externes")
    df = pd.DataFrame(columns=BDSOLU_SITE_IDENTIFIANTS_EXTERNES_COLUMNS.keys())

    for (
        bdsolu_col,
        intermediate_col,
    ) in BDSOLU_SITE_IDENTIFIANTS_EXTERNES_COLUMNS.items():
        if intermediate_col is not None:
            df[bdsolu_col] = df_intermediate[intermediate_col]

    return df


def obtain_data(input_path: str):
    df_site = fill_site(input_path)
    df_site_id_externes = fill_site_id_externes(input_path)
    df_sondage = fill_sondage(input_path)
    df_sondage_niveau = fill_sondage_niveau(input_path)
    df_sondage_niveau_materiau = fill_sondage_niveau_materiau(input_path)
    df_echantillon = fill_echantillon(input_path)
    df_analyse = fill_analyse(input_path)

    return (
        df_site,
        df_site_id_externes,
        df_sondage,
        df_sondage_niveau,
        df_sondage_niveau_materiau,
        df_echantillon,
        df_analyse,
    )


def export_csv(
    df_site,
    df_site_id_externes,
    df_sondage,
    df_sondage_niveau,
    df_sondage_niveau_materiau,
    df_echantillon,
    df_analyse,
    output_path,
):
    dataframes = [
        (df_site, "SITE"),
        (df_site_id_externes, "SITE_IDENTIFIANTS_EXTERNES"),
        (df_sondage, "SONDAGES"),
        (df_sondage_niveau, "SONDAGE_NIVEAUX"),
        (df_sondage_niveau_materiau, "SONDAGE_NIVEAUX_MATERIAUX"),
        (df_echantillon, "ECHANTILLONS"),
        (df_analyse, "ANALYSES"),
    ]

    try:
        with pd.ExcelWriter(os.path.join(output_path)) as writer:
            for df, sheet_name in dataframes:
                df.to_excel(writer, index=False, sheet_name=sheet_name)
        print(f"Dataframes exported to {output_path}.")
    except Exception as e:
        print(
            f"An error occurred while exporting dataframes to {output_path}: {str(e)}"
        )
