import os
import pandas as pd
from datetime import datetime
from . import bappet_bappop
from . import bdsolu


def export_to_excel(df: pd.DataFrame, filename: str, directory: str = "data/output"):
    if df.empty:
        raise ValueError("DataFrame is empty.")

    if not os.path.exists(directory):
        raise FileNotFoundError(
            f"The specified directory '{directory}' does not exist."
        )

    if not os.access(directory, os.W_OK):
        raise PermissionError(f"The specified directory '{directory}' is not writable.")

    datetime_str = datetime.now().strftime("%Y%m%d_%H%M%S")
    file_path = os.path.join(directory, f"{filename}_{datetime_str}.xlsx")

    try:
        df.to_excel(file_path)
        print(f"DataFrame exported to {file_path}.")
    except Exception as e:
        print(f"An error occurred while exporting DataFrame to {file_path}: {str(e)}")


def run(df_lexique: pd.DataFrame, intermediate_folder: str, output_folder: str):
    # Create final dataframe and export to excel
    bdd_interne_path = os.path.join(intermediate_folder, "intermediate_complete.xlsx")

    # For bappet and bappop
    bdd_interne_df_veg = bappet_bappop.read_excel(bdd_interne_path, "analyses_veg")
    bdd_interne_df_sol = bdsolu.read_excel(bdd_interne_path, "echantillons_sol")

    # obtain data from excel file
    data = bappet_bappop.obtain_data(bdd_interne_df_veg, bdd_interne_df_sol)

    # export data to excel
    bappet_bappop_output_path = os.path.join(output_folder, "bappet_bappop.xlsx")
    bappet_bappop.write_data_to_excel(
        data, df_lexique, bdd_interne_path, bappet_bappop_output_path
    )

    # For bdsolu

    bdsolu_output_path = os.path.join(output_folder, "bdsolu.xlsx")
    (
        df_site,
        df_sondage,
        df_sondage_niveau,
        df_sondage_niveau_materiau,
        df_echantillon,
        df_analyse,
    ) = bdsolu.obtain_data(bdd_interne_path)

    bdsolu.export_csv(
        df_site,
        df_sondage,
        df_sondage_niveau,
        df_sondage_niveau_materiau,
        df_echantillon,
        df_analyse,
        bdsolu_output_path,
    )
    return 200
