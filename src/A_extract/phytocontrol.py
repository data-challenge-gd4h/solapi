import pandas as pd
from datetime import datetime
from .main import InputFile

COLUMNS_TO_CAST = [
    "incertitude_labveg",
    "lq_labveg",
]


class PhytocontrolFile(InputFile):
    def __init__(self, filename: str):
        super(PhytocontrolFile, self).__init__()
        self.filename = filename

    def parse_date(self, date: str) -> datetime:
        try:
            return datetime.strptime(date, "%Y-%m-%d %H:%M:%S")
        except:
            return None

    def _clean_data(self, df: pd.DataFrame) -> pd.DataFrame:
        # Make column names lowercase
        df.columns = map(str.lower, df.columns)

        # Replace comma with dot
        for col in COLUMNS_TO_CAST:
            df[col] = df[col].apply(
                lambda x: float(x.replace(",", ".")) if isinstance(x, str) else x
            )

        # df.resultat = df.resultat.apply(lambda x: x.replace(",", ".").replace("D", "").replace("+/- 0", "").strip())

        # Replace NaN with None
        df = df.where(pd.notnull(df), None)

        # Change column types
        df = df.astype({"code_sandre_labveg": "Int64"})
        df.code_sandre_labveg = df.code_sandre_labveg.astype(object).replace(
            pd.NA, None
        )
        df.rapport_date_labveg = df.rapport_date_labveg.apply(self.parse_date)
        df.echantillon_date_reception_labveg = (
            df.echantillon_date_reception_labveg.apply(self.parse_date)
        )

        return df
