import pandas as pd
from .main import InputFile


class AgrolabFile(InputFile):
    def __init__(self, filename: str):
        super(AgrolabFile, self).__init__()
        self.filename = filename

    def _clean_data(self, df: pd.DataFrame) -> pd.DataFrame:
        # Change column types
        df = df.astype(
            {
                # "betrnr": "Int64",
                # "aufnr": "Int64",
                # "ee_analynr": "Int64",
                "numero_echantillon_labsol": "Int64",
                # "code_agrolab": "Int64",
                "code_sandre_labsol": "Int64",
                "code_labsol": "Int64",
            }
        )

        # Replace NaN with None
        df = df.where(pd.notnull(df), None)

        # Replace pd.Na in code_sandre_labsol with 0
        df = df.replace(pd.NA, None)
        df.code_sandre_labsol = df.code_sandre_labsol.replace(pd.NA, 0)

        return df
