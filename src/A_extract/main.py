import pandas as pd
from abc import ABC, abstractmethod


class InputFile(ABC):
    def __init__(self):
        self.name = self.__class__.__name__.lower()
        self.filepath = "data/input/"

    def extract(self) -> pd.DataFrame:
        try:
            df = pd.read_excel(self.filepath + self.filename)
            return self._clean_data(df)
        except FileNotFoundError as e:
            print(f"Error file not found: {e}")
        except Exception as e:
            print(f"Error: {e}")

    @abstractmethod
    def _clean_data(self, df: pd.DataFrame) -> pd.DataFrame:
        pass
