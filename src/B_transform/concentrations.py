import pandas as pd

from .base import combine_duplicates


# Agrolab
def get_concentration_sign_agrolab(x: pd.Series, code_sandre: int):
    # Compute s_... columns
    if x.code_sandre_labsol == code_sandre:
        if "<" in x.concentration_texte_labsol:
            return "<"
        elif ">" in x.concentration_texte_labsol:
            return ">"
    else:
        return None


def get_concentration_value_agrolab(x: pd.Series, code_sandre: int):
    # Compute c_... columns
    if x.code_sandre_labsol == code_sandre:
        return x.concentration_nombre_labsol
    else:
        return None


def compute_concentrations_agrolab(
    df_agrolab: pd.DataFrame, df_lexique: pd.DataFrame
) -> pd.DataFrame:
    # Initialisation des colonnes fixes
    df = pd.DataFrame(
        {
            "id_site": None,
            "id_maille": None,
            "ref_echantillon_labsol": df_agrolab.numero_echantillon_labsol,
            "nature_echantillon": df_agrolab.nature_echantillon_labsol.apply(
                lambda x: "Sol" if x == "TS" else x
            ),
            "commentaires_analyses": None,
        }
    )

    temp_dict = {}
    molecule_names = [
        name.lower() for name in df_lexique.nom_molecule_normalized.unique()
    ]

    for col in molecule_names:
        # Get the code_sandre value, handling NaN by substituting a default (e.g., None)
        code_sandre_value = df_lexique[
            df_lexique.nom_molecule_normalized == col
        ].code_sandre.iloc[0]

        # Handle NaN by skipping or providing a default value
        if pd.isna(code_sandre_value):
            continue  # Skip processing this molecule if the value is NaN
            # Or use a default value instead:
            # code_sandre_value = 0

        rounded_value = round(code_sandre_value)

        # Apply the logic with the rounded value
        temp_dict[f"s_{col}"] = df_agrolab.apply(
            lambda x: get_concentration_sign_agrolab(x, rounded_value),
            axis=1,
        )
        temp_dict[f"c_{col}"] = df_agrolab.apply(
            lambda x: get_concentration_value_agrolab(x, rounded_value),
            axis=1,
        )

    temp_df = pd.DataFrame(temp_dict)

    # Fusionner les colonnes calculées avec les colonnes fixes
    df = pd.concat([df, temp_df], axis=1)

    # Combiner les doublons
    return combine_duplicates(df.copy(), ["ref_echantillon_labsol"])


# Phytocontrol
def clean_concentration_phytocontrol(x: pd.Series) -> float:
    concentration_str = str(x.concentration_texte_labveg)
    # Keep characters before "+/-"
    concentration_str = concentration_str.split("+/-")[0]
    # Remove the "D"
    concentration_str = concentration_str.replace("D", "")
    # Remove < and >
    concentration_str = concentration_str.replace("<", "").replace(">", "")
    # Replace , with .
    concentration_str = concentration_str.replace(",", ".")
    # Remove ND
    concentration_str = concentration_str.replace("ND", "")
    concentration_str.strip()

    try:
        # Convert concentration string to float
        concentration = float(concentration_str)
    except ValueError:
        # Return NaN if conversion fails
        concentration = float("nan")

    return concentration


def get_concentration_sign_phytocontrol(x: pd.Series, code_sandre: int):
    # Compute s_... columns
    try:
        if (
            x.code_sandre_labveg is not None
            and int(x.code_sandre_labveg) == code_sandre
            and (
                "<" in x.concentration_texte_labveg
                or ">" in x.concentration_texte_labveg
            )
        ):
            return "<" if "<" in x.concentration_texte_labveg else ">"
        else:
            return None
    except TypeError:
        return None


def get_concentration_value_phytocontrol(x: pd.Series, code_sandre: int):
    # Compute c_... columns
    try:
        if (
            x.code_sandre_labveg is not None
            and int(x.code_sandre_labveg) == code_sandre
        ):
            concentration = clean_concentration_phytocontrol(x)
            if x.unite_analyse_labveg == "µg/kg":
                return concentration / 1000
            else:
                return concentration
    except (TypeError, AttributeError):
        print("Error: Invalid data or missing attributes")
        print(
            "Result",
            x.code_sandre_labveg,
            int(x.code_sandre_labveg),
            code_sandre,
        )
        print("Concentration", clean_concentration_phytocontrol(x))
        print("*" * 50)
    return None


def compute_concentrations_phytocontrol(
    df_phytocontrol: pd.DataFrame, df_lexique: pd.DataFrame
) -> pd.DataFrame:
    # Initialisation des colonnes fixes
    df_fixed = pd.DataFrame(
        {
            "id_site": [None] * len(df_phytocontrol),
            "id_maille": [None] * len(df_phytocontrol),
            "ref_echantillon_labveg": df_phytocontrol.numero_echantillon_labveg,
            "nature_echantillon": ["Légume"] * len(df_phytocontrol),
        }
    )

    # Création des colonnes calculées dans un dictionnaire temporaire
    temp_dict = {}
    molecule_names = [
        name.lower() for name in df_lexique.nom_molecule_normalized.unique()
    ]

    for col in molecule_names:
        # Get the code_sandre value, handling NaN by substituting a default (e.g., None)
        code_sandre_value = df_lexique[
            df_lexique.nom_molecule_normalized == col
        ].code_sandre.iloc[0]

        # Handle NaN by skipping or providing a default value
        if pd.isna(code_sandre_value):
            continue  # Skip processing this molecule if the value is NaN
            # Or use a default value instead:
            # code_sandre_value = 0

        code_sandre = round(code_sandre_value)

        # Apply the logic with the rounded value
        temp_dict[f"s_{col}"] = df_phytocontrol.apply(
            lambda x: get_concentration_sign_phytocontrol(x, code_sandre),
            axis=1,
        )
        temp_dict[f"c_{col}"] = df_phytocontrol.apply(
            lambda x: get_concentration_value_phytocontrol(x, code_sandre),
            axis=1,
        )

    # Conversion du dictionnaire temporaire en DataFrame
    df_calculated = pd.DataFrame(temp_dict)

    # Concaténer les colonnes fixes et calculées
    df = pd.concat([df_fixed, df_calculated], axis=1)

    # Suppression des doublons
    return combine_duplicates(df.copy(), ["ref_echantillon_labveg"])
