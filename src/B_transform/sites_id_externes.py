import pandas as pd
from constants import SITE_IDENTIFIANTS_EXTERNES_COLUMNS


def compute_sites_id_externes() -> pd.DataFrame:
    return pd.DataFrame(columns=SITE_IDENTIFIANTS_EXTERNES_COLUMNS)
