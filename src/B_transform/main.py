import os
import pandas as pd
from .concentrations import (
    compute_concentrations_agrolab,
    compute_concentrations_phytocontrol,
)
from .echantillons import (
    compute_echantillons_agrolab,
    compute_echantillons_phytocontrol,
)
from .mailles import compute_mailles
from .sites import compute_sites
from .sites_id_externes import compute_sites_id_externes


def run(
    df_agrolab: pd.DataFrame, df_phytocontrol: pd.DataFrame, df_lexique: pd.DataFrame
):
    # Sites
    df_sites = compute_sites()

    # Sites identifiants externes
    df_sites_id_externes = compute_sites_id_externes()

    # Mailles
    df_mailles = compute_mailles()

    # Echantillons
    df_echantillons_agrolab = compute_echantillons_agrolab(df_agrolab)
    df_echantillons_phytocontrol = compute_echantillons_phytocontrol(df_phytocontrol)

    # Concentrations
    df_concentrations_agrolab = compute_concentrations_agrolab(df_agrolab, df_lexique)
    df_concentrations_phytocontrol = compute_concentrations_phytocontrol(
        df_phytocontrol, df_lexique
    )

    return (
        df_sites,
        df_sites_id_externes,
        df_mailles,
        df_echantillons_agrolab,
        df_echantillons_phytocontrol,
        df_concentrations_agrolab,
        df_concentrations_phytocontrol,
    )


def export_csv(
    df_sites: pd.DataFrame,
    df_sites_id_externes: pd.DataFrame,
    df_mailles: pd.DataFrame,
    df_echantillons_agrolab: pd.DataFrame,
    df_echantillons_phytocontrol: pd.DataFrame,
    df_concentrations_agrolab: pd.DataFrame,
    df_concentrations_phytocontrol: pd.DataFrame,
    file_path: str,
    file_name: str,
):
    if (
        df_echantillons_agrolab.empty
        or df_echantillons_phytocontrol.empty
        or df_concentrations_agrolab.empty
        or df_concentrations_phytocontrol.empty
    ):
        raise ValueError("At least one DataFrame is empty.")

    full_file_path = os.path.join(file_path, file_name)

    if not os.path.exists(file_path):
        raise FileNotFoundError(f"Specified file path '{file_path}' does not exist.")

    if not os.access(file_path, os.W_OK):
        raise PermissionError(f"Specified file path '{file_path}' is not writable.")

    dataframes = [
        (df_sites, "sites"),
        (df_sites_id_externes, "site_identifiants_externes"),
        (df_mailles, "mailles"),
        (df_echantillons_agrolab, "echantillons_sol"),
        (df_echantillons_phytocontrol, "echantillons_veg"),
        (df_concentrations_agrolab, "analyses_sol"),
        (df_concentrations_phytocontrol, "analyses_veg"),
    ]

    try:
        with pd.ExcelWriter(os.path.join(full_file_path)) as writer:
            for df, sheet_name in dataframes:
                df.to_excel(writer, index=False, sheet_name=sheet_name)
        print(f"Dataframes exported to {full_file_path}.")
    except Exception as e:
        print(
            f"An error occurred while exporting dataframes to {full_file_path}: {str(e)}"
        )
