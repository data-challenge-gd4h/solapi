import pandas as pd


def combine_duplicates(
    df: pd.DataFrame, groupby_columns: list, sort=True
) -> pd.DataFrame:
    # Sort the DataFrame by the groupby columns if needed
    if sort:
        df.sort_values(by=groupby_columns).reset_index(drop=True)

    # Group the DataFrame by the groupby columns and keep the first occurrence of remaining columns
    result_df = df.groupby(groupby_columns).first().reset_index()

    return result_df.copy()
