import pandas as pd
from constants import MAILLES_COLUMNS


def compute_mailles() -> pd.DataFrame:
    return pd.DataFrame(columns=MAILLES_COLUMNS)
