import pandas as pd
from datetime import datetime

from constants import ECHANTILLONS_PHYTOCONTROL_COLUMNS, ECHANTILLONS_AGROLAB_COLUMNS

from .base import combine_duplicates


def compute_echantillons_agrolab(df_agrolab: pd.DataFrame) -> pd.DataFrame:
    # À compléter / améliorer

    df = pd.DataFrame(columns=ECHANTILLONS_AGROLAB_COLUMNS)

    # id_maille
    df.id_maille = None

    # "annee_analyse"
    df.annee_analyse = df_agrolab.date_echantillonnage_labsol.apply(lambda x: x.year)

    # "date_echantillonnage"
    df.date_echantillonnage = df_agrolab.date_echantillonnage_labsol.apply(
        lambda x: datetime.date(x)
    )

    # "heure_echantillonnage"
    df.heure_echantillonnage = df_agrolab.date_echantillonnage_labsol.apply(
        lambda x: x.hour
    )

    # "nature_echantillon"
    df.nature_echantillon = df_agrolab.nature_echantillon_labsol.apply(
        lambda x: "Sol" if x == "TS" else x
    )

    # "date_analyse_labsol"
    df.date_analyse_labsol = df_agrolab.date_analyse_labsol.apply(
        lambda x: datetime.date(x)
    )

    # "ref_echantillon_labsol"
    df.ref_echantillon_labsol = df_agrolab.numero_echantillon_labsol

    # "nom_echantillon_projet"
    df.nom_echantillon_projet = df_agrolab.nom_echantillon_labsol

    # "laboratoire_labsol"
    df.laboratoire_labsol = df_agrolab.reference_labo_labsol

    # "tamisage_sol"
    df.tamisage_sol = df_agrolab.code_sandre_labsol.apply(
        lambda x: "Oui" if x == 4224 else "Non"
    )

    # "extraction_sol"
    df.extraction_sol = df_agrolab.code_sandre_labsol.apply(
        lambda x: "Oui" if x == 7227 else "Non"
    )

    # "matiere_seche_sol"
    df.matiere_seche_sol = df_agrolab.apply(
        lambda x: x.concentration_texte_labsol
        if x.code_sandre_labsol == 9449
        else None,
        axis=1,
    )

    # df.methode_preparation_analyse_un_bdsolu
    # df.methode_preparation_analyse_deux_bdsolu
    # df.nom_preparation_analyse_un_bdsolu
    # df.nom_preparation_analyse_deux_bdsolu
    # df.incertitude_analyse_labsol

    df = df.astype({"annee_analyse": "Int64", "heure_echantillonnage": "Int64"})

    return combine_duplicates(df, ["ref_echantillon_labsol"])


def compute_echantillons_phytocontrol(df_phytocontrol: pd.DataFrame) -> pd.DataFrame:
    # À compléter / améliore

    df = pd.DataFrame(columns=ECHANTILLONS_PHYTOCONTROL_COLUMNS)

    # id_maille
    df.id_maille = None

    # auteur_fiche_bappop
    df.auteur_fiche_bappop = None

    # "annee_analyse"
    df.annee_analyse = df_phytocontrol.echantillon_date_reception_labveg.apply(
        lambda x: x.year
    )

    # commentaires_labveg
    df.commentaires_labveg = df_phytocontrol.commentaire_rapport_labveg

    # "date_echantillonnage"
    df.date_echantillonnage = df_phytocontrol.echantillon_date_reception_labveg.apply(
        lambda x: x.date()
    )

    # "heure_echantillonnage"
    df.heure_echantillonnage = df_phytocontrol.echantillon_date_reception_labveg.apply(
        lambda x: x.hour
    )

    # "nature_echantillon"
    df.nature_echantillon = "Légume"

    # "date_analyse_labveg"
    df.date_analyse_labveg = df_phytocontrol.rapport_date_labveg.apply(
        lambda x: x.date()
    )

    # "ref_echantillon_labo"
    df.ref_echantillon_labveg = df_phytocontrol.numero_echantillon_labveg

    # "nom_echantillon_projet"
    df.nom_echantillon_projet = df_phytocontrol.nom_echantillon_labveg

    # laboratoire_leg
    df.laboratoire_leg = "Phytocontrol"

    # espece_leg
    df.espece_leg = df_phytocontrol.type_matrice_labveg

    # unité veg
    df.unite_veg = df_phytocontrol.unite_analyse_labveg

    df = df.astype({"annee_analyse": "Int64"})

    return combine_duplicates(df, ["ref_echantillon_labveg"])
